#include "../inc/user.h"

//create a user
User* createUser(char* name, char* path){

  User* user = malloc(sizeof(User));
  user->userName = name;
  user->userPath = path;
}
//set user file pointer to current notebook
int userSetCurrentFile(User* user, char* file){
  
  //TODO find better way, could lead to buffer overflow
  char dest[200];
  strcpy(dest, user->userPath);
  strcat(dest,file);
 printf("%s\n",dest); 
 //TODO F_OK is non-functional on windows check into
  if(access(dest, F_OK)==0){
    //so user file has valid pointer
    user->userFile = (char*) malloc(200);
    strcpy(user->userFile,dest);
  }
  else{

    printf("%s is not a notebook\n",file);
    return 0;
  }
  return 1;
}

//input selection for userNotebook
void userSelectNotebook(User* user){

  DIR* d;
  char buffer[50];
  struct dirent *dir;

  d = opendir(user->userPath);//get dir of users files

  if (d){

    printf("select NoteBook: \n");
    //list all files in userpath
    while((dir = readdir(d)) != NULL){

      if(dir->d_name){

      printf("%s\n", dir->d_name);
      }
    }

    closedir(d);  
  }
  //TEMPORARY current method for selecting file
  //Must get changed by UI process later
  printf("open: ");
  scanf("%49s", buffer);//CHECK is 49 chars sufficient?
  if(userSetCurrentFile(user,buffer)){
    user->userNotebook = buffer;
    printf("user Notebook has been changed to: %s\n",buffer);
  }
  else{
    printf("%s not found\n",buffer);
  }
}

char* userReadFile(User* user){

  char* buffer = 0;
  long length;

  FILE* fp = fopen(user->userFile,"rb");
  printf("%s userFile\n",user->userFile);
  if(access(user->userFile, F_OK)==0){
    //fseek ftell and fread get size of file and allocate mem
    fseek(fp,0,SEEK_END);
    length = ftell (fp);
    fseek(fp,0,SEEK_SET);
    buffer = malloc(length);
    fread(buffer,1,length,fp);
    fclose(fp);
    return buffer;
    
  }
  else{
    printf("%s is not a file\n");
    fclose(fp);
    return buffer;
  }
}
  






