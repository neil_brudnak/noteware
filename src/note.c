#include "../inc/note.h"

//create a note
Note* createNote(Topic* topic,char* name){

  Note* note = malloc(sizeof(Note));
  note->noteName = name;
  time_t _t = time(NULL);
  note->noteDate = asctime(localtime(&_t));
  //temporary
  note->noteConts = "these are the contents";
  if(topic==NULL)return note;//if root of tree
  note->noteTopic = topic;//if node on tree
  return note;
}
//create a topic
Topic* createTopic(Topic* parent,char* name){

  Topic* topic = malloc(sizeof(Topic));
  topic->topicName = name;
  if(parent==NULL) return topic;//if root of tree
  if(parent->topicLine==NULL){
    parent->topicLine = topic;//if node on tree
    return topic;
  }
  //TODO set up error handeling for stuff like this
}
//create a subtopic 
Topic* createSubTopic(Topic* parent,char* name){
  Topic* topic = malloc(sizeof(Topic));
  topic->topicName = name;
  if (parent->subLine==NULL){
    parent->subLine = topic;
    return topic;
  }
}

//Temporary need to fix
char* topicGenerateId(Topic* parent,Topic* topic){
  
  char* parentTopicId = parent->_id;
  int depth=0, i=0;
  while (parentTopicId[i] != '\0'){
    depth++;
  }
  printf("%d\n", depth);
}



//testing func
void _note_print(Note* note){

  printf("noteName: %s\nnoteDate: %s\nnoteTopic: %s\nnoteConts: \n%s\n",note->noteName, 
  note->noteDate,
  note->noteTopic->topicName,
  note->noteConts);
}

