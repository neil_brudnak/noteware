CC = gcc
BIN = noteware
ODIR = obj
SDIR = src
CFLAGS = -Iinc -lm -Wall -g
OBJS := $(patsubst $(SDIR)/%.c,$(ODIR)/%.o, $(wildcard $(SDIR)/*.c))

$(ODIR)/%.o: $(SDIR)/%.c
	$(CC) -c $< -o $@ -g

$(BIN): $(OBJS)
	$(CC) $^ -o $@ $(CFLAGS)

.PHONY: clean
clean:
	rm -f $(OBJS) qlex

