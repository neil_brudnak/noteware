#ifndef NOTE_H
#define NOTE_H
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

//struct for organizing notes
typedef struct TOPIC{
  
  char* _id;
  char* topicName; //data
  struct TOPIC* topicLine;//left branch
  struct TOPIC* subLine;//right branch
}Topic;//b-tree sub-structure 

//struct for notes
typedef struct NOTE{

  char* noteName;
  char* noteDate;
  char* noteConts;
  Topic* noteTopic; 
}Note;


Note* createNote(Topic* topic,char* name);
Topic* createTopic(Topic* parent, char* name);
Topic* createSubTopic(Topic* parent, char* name);
char* topicGenrateId(Topic* topic);
void _note_print(Note* note);
#endif
