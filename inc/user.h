#ifndef USER_H
#define USER_H
#include <dirent.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include "note.h"

//struct for holding active user data
typedef struct USER{

  char* userName;
  char* userPath;
  char* userNotebook;
  char* userFile;
  Topic* userTopicRoot;
  Topic* userTopicCurrent;
  Topic* userTopicSubTree;
}User;

User* createUser(char* name, char* path);
int userSetCurrentFile(User* user, char* file);
void userSelectNotebook(User* user);
char* userReadFile(User* user);
#endif
